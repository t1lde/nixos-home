{config,  pkgs,  ... }: 
with import <nixpkgs> {};
with builtins;
{

  home.file.".config/awesome/lain".source = fetchFromGitHub {
    owner = "lcpz";
    repo = "lain";
    rev = "f1964b40e4";
    sha256 = "1qhigwg70nc9qgina9s8nvz35y68v30f6zsjrc0nhlizxj5qc7a8";
  };

  home.file.".config/awesome/volume-control".source = fetchFromGitHub {
    owner = "deficient";
    repo = "volume-control";
    rev = "137b19e";
    sha256 = "1xsxcmsivnlmqckcaz9n5gc4lgxpjm410cfp65s0s4yr5x2y0qhs";
  };
  
  # TODO: Find a better way to manage awesomewm conf from
  # Nix, inc. more separated concerns (may require an awesomewm package)
  home.file.".config/awesome/rc.lua".source = ./rc.lua;

  home.file.".config/awesome/theme.lua".source = ./theme.lua;

}
