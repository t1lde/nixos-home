# I cant figure out how to get rust working under nixos :(
let 
  moz_overlay = import (
    builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz
  );

  nixpkgs = import <nixpkgs> {overlays = [moz_overlay];};
  ruststable = (nixpkgs.latest.rustChannels.stable.rust.override { extensions = [ "rust-src" "rls-preview" "rust-analysis" "rustfmt-preview" ];});


in
  with nixpkgs;
rustPlatform.buildRustPackage rec {
  name = "tweet_delete_cli-${version}";
  version = "0.0.1";
  src = builtins.fetchGit {
    url = "https://gitlab.com/t1lde/tweet_delete_cli.git";
    ref = "${version}";
  };

  buildInputs = [openssl];
  nativeBuildInputs = [ruststable rustc cargo pkgconfig];

  cargoSha256 = "0fb248fckknnhgqgr3h333qy291y8d55faczkra0ir9l27nipvna";


}
