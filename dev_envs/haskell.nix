{config, pkgs, ...}:

{
  home.packages = with pkgs; with haskellPackages; [
    ghc stack 

    # Spacemacs layer dependencies
    hoogle 
    apply-refact hlint stylish-haskell hasktags ghc-mod intero
  ];
}
