self: super: 

{
  airsonic = super.airsonic.overrideAttrs ( old: rec {
    version = "10.4.0";
    name = "airsonic-${version}";
    src = super.fetchurl {
      url = "https://github.com/airsonic/airsonic/releases/download/v${version}/airsonic.war";
      sha256 = "1aj7z5w6i9iisn96p1n0h487mlpw59mrg0xg50lr4ndp95q85cd7";
    };
  });
}
