self: super: 
let 
  py = self.python3.pkgs;
in
{
  # Chromecast commandline client

  catt = py.buildPythonApplication rec {
    pname = "catt";
    version =  "0.9.5";
    name = "${pname}-${version}";

    src = py.fetchPypi {
      inherit pname version;
      sha256 = "a2c6a1803a916766aa5792d7467dd6e043fe48d3ddaf701b766324a1efd5a432";
    };

    doCheck = false;
    propagatedBuildInputs = with py; [ 
      PyChromecast 
      youtube-dl 
      click
      requests
      ifaddr
    ];

  };



}
