self: super:
let 
  # Override python for python3 support, required by youcompleteme
  vim_configurable = self.vim_configurable.override {
    python = self.python3;
  };
in
{

  #vimPlugins.vim-carbon-now-sh = self.vimUtils.buildVimPluginFrom2Nix {
  #  name = "vim-carbon-now-sh"; 
  #  src = self.fetchFromGitHub {
  #    owner = "kristijanhusak";
  #    repo = "vim-carbon-now-sh";
  #    rev = "789b15d17966a1100ed2889d670923dd6d9ff063";
  #    sha256 = "1vfhdqv8mf8w0s4nv8k2rqzvahvh1lxm4zsd3ks1n334f580w8x4";
  #  };
  #  dependencies = [];
  #  
  #};
  
  vim-conf = vim_configurable.customize {
      name = "vim";
      vimrcConfig = {
        packages.vim-conf = {
          start = with super.vimPlugins; [
            YouCompleteMe
            nerdtree
            syntastic
            vim-javacomplete2
            vim-airline
            vim-airline-themes
            vim-nix
            vim-addon-nix
          ];
        };

      customRC = ''
	  "Load vim default settings
	  set nocompatible

	  " allow backspacing over everything in insert mode
	  set backspace=indent,eol,start
	  
	  filetype plugin indent on
	  
	  " spaces >>>>> tabs
	  set tabstop=4
	  set softtabstop=4
	  set shiftwidth=4
	  set expandtab
	  
	  set history=50		" keep 50 lines of command line history
	  set ruler	    	" show the cursor position all the time
	  set showcmd		    " display incomplete commands
	  set incsearch		" do incremental searching
	  
	  set foldmethod=indent
	  set foldnestmax=10
	  set nofoldenable
	  set foldlevel=2
	  set laststatus=2
	  
	  " In many terminal emulators the mouse works just fine, thus enable it.
	  if has('mouse')
	    set mouse=a
	  endif
	  
	  syntax on
	  
	  set ignorecase
	  set smartcase
	  let mapleader=","
	  set number
	  
	  " Saves file with sudo, with prompt, even if vim is not opened with sudo
	  cmap w!! w !sudo tee > /dev/null %
	  	  
	  	  
	  let g:syntastic_cpp_compiler_options = ' -std=c++11'
	  let g:polyglot_disabled = ['python']
	  let g:indentLine_char="¦"
	  let g:indentLine_color_term = 239
	  let g:airline_powerline_fonts = 1
	  let g:airline#extensions#hunks#non_zero_only = 1
	  let g:airline#extensions#tabline#enabled = 1
	  let g:airline#extensions#tabline#show_buffers = 0
	  let g:airline_theme='distinguished'
	  
	  nmap c-space c x c o c p  
      '';  
      };

  };
}
