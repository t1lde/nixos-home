{ config, pkgs, ...} :
{
  home.file.".config/sway/config".source = ./sway.conf;
  home.file.".config/sway/wallpaper.png".source = ./wallpaper.png;

  home.file.".config/waybar/config".source = ./waybar.conf;
  home.file.".config/waybar/style.css".source = ./waybar.css; 
  
  # Start sway on login
  programs.zsh.loginExtra = ''
    if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
      XKB_DEFAULT_LAYOUT=us exec sway
    fi
  '';

  # Use native wayland support where possible
  home.sessionVariables = {
    QT_QPA_PLATFORM = "wayland-egl";
    CLUTTER_BACKEND = "wayland";
    SDL_VIDEODRIVER = "wayland";
  };

  # Extra packages 
  home.packages = with pkgs; [
    waybar slurp grim
  ];
}
